function out = GA_connector(network, problem, params)
%CALCULATE_POWER_FROM_TREE Summary of this function goes here
%   Detailed explanation goes here

%% This Script optimizes the power-consumption of a ventilation-System by using a genetic algorithm
global vals;
global iter;

      plot_1_or_0 = 0;

      FitFcn = @(x)Cost_Function_connection(network, x,  plot_1_or_0).Cost;
      
%       init_mat = initialize_semi_random(params.Init_type, problem.VarMin, problem.VarMax, problem.nVar, params.nPop);
      init_mat = initialize_semi_random(network, params, problem);

      
      
    options = optimoptions('ga','PopulationSize', params.nPop, 'InitialPopulationMatrix', init_mat, 'MaxGenerations', params.MaxIt, 'InitialPopulationRange', [2;3.999], 'PlotFcn', {@gaplotbestf @gaplotbestindiv} ) ;
     [x, fval] = ga(FitFcn,problem.nVar, options);
     
%      Best_costs_GA = score
     disp(fval)
%       [x, fval] = ga(Cost_Function_connection,problem.nVar);
%     disp(vals);
    BestCostsGA = [];
    % Create_vector of only the best solutions:
    calculations = size(vals, 1);
    Generations = calculations / params.nPop;
    individuals = params.nPop;
    
    Best = 9999999;
    global_best = [];
    for Generation = 1:Generations
        for Individual = 1:params.nPop
            calculation = (Generation -1)*params.nPop + Individual;
            Best = min([Best vals(calculation)]);
            BestCostsGA(Generation,1) = Best;
            global_best = Best;
        end 
    end 
    
    fill_up = params.MaxIt - size(BestCostsGA,1);
    fill_up = repmat(global_best, fill_up, 1);
    
    [sA1, sA2] = size(BestCostsGA);
     [sB1, sB2] = size(fill_up);
     C(sA1+1:sA1+sB1, 1) = fill_up;
     C(1:sA1, 1)         = BestCostsGA;
    BestCostsGA = C;
    
    out = BestCostsGA;
    
    
    
end 