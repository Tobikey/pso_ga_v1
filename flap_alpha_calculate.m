function [result] = flap_alpha_calculate(dp, ID_flaps, V, M)
%FLAP_ALPHA_CALCULATE This function takes the dp-matrix and does the
%following:
%   1. if the sum of all dp's in one mesh > 0 Then impossible solition (possible = 0)
%   2. if possible ==1 then it calculates all alpha-values to satisfy the
%   equation sum(all dp's in one mesh) ==0.

%%   important variables
    verbosity = 0; % if verbosity = 1 then output for debugging is increased. 
    rows = size(dp, 1);
    columns = size(dp,2);
    dp_sum = zeros(rows, 1);
%     ID_flaps = zeros(1, columns); % the resulting alphas are stored here
        
%%   sum of all pressures
    for row = 1:rows
       for column = 1:columns
           dp_sum(row,1) = dp_sum(row,1) + dp(row, column);
       end 
    end
    
    % Check for impossible solution
    if max(dp_sum)>0 
        if verbosity > 0
            disp("Vents not strong enough to deliver volume-flow")
            disp(dp_sum + " > 0");
        end 
        result.possible = 0;
        result.ID_flaps = zeros(1, columns);
        result.dp = dp;
    % calculate angles
    
    else
        result.possible = 1;
        
        %Basic flap-equation:    
        %   dp_klappe =  300./((alpha.^2)/90).*V^2
        %   equation resovled to alpha:
        %   alpha = (sqrt(300/(dp*90)*V^2)
        
        V = V.*ID_flaps;
        
        V_tmp = V;
        
        % create matrix with volume flows of flaps
        for i = 1:rows-1
            V = [V;V_tmp];
        end 
            V = V.*M;
       
        alpha = abs(sqrt(300*90./(-dp_sum).*V.^2));
        
%         alpha = alpha.*M
        dp_tmp = (300./((alpha.^2+0.00000001)/90).*V.^2);
        result.dp = dp + dp_tmp;
        
        alpha = collapse_mat_to_vec(alpha);

        result.ID_flaps = ID_flaps.*5 + alpha/1000  ; %Add identifier 5 (flap) in front of angle digit
        
    end





end

