function [SOL] = Cost_Function_calculation(network, show_dp_1_0)
%CALCULATE_POWER_FROM_TREE Summary of this function goes here
%   Detailed explanation goes here

%% This Script calculates a the power-consumption of a ventilation-System.

%-------------------------------------------------------------------------%
% INPUT: 1. Tree-structure of the ventilationsystem (mesh-matrix)
%        2. Vector which defines the elements in the system (ID-vektor)
%        3. Vector with the volume flows for each outlet
% OUTPUT: 1. boolean (volume flows can be reached)
%         2. Power-consuption of fans
%-------------------------------------------------------------------------%
   

%% Important Variables
%   Import:
    M  = network.M;
    ID = network.ID; 
    k  = network.k;
    
%   Verbosity for debugging
    verbosity = 0; % if verbosity  > 0 then output for debugging is increased. 
   
%   Size of the mesh-matrix 
    rows = size(M, 1);
    columns = size(M, 2);
    
%   ID-Convention
    ID_Fan = network.ID_Fan;
    ID_Res = network.ID_Res;
    ID_Flap = network.ID_Flap;
    
    
%   dp_vector
    dp = zeros(rows, columns);
    
%% Volumeflow
%     Calculate volumeflow elementwise (vector)
      V = (M'*k)';
      V_sq = V.^2;     
      
%% Pressure difference dp

%   Resistance-Elements: Calculate pressure difference
        ID_res_1or0 = filter_matrix(ID, ID_Res, ID_Res + 1);    % Positions with resistance-elements are identified
        dp = dp + V_sq.*ID_res_1or0*(50);         % dp_res calculated with formula: dp = 50*V^2 and added to dp-matrix
      
%   Fan-Elements: calculate pressure difference
        ID_fan_1or0 = filter_matrix(ID, ID_Fan, ID_Fan + 1);    % Positions with fan-elements are identified
        ID_fan_vec = filter_matrix(ID, ID_Fan, ID_Fan + 1).*ID;     % Values of fan-elements written, rest is filtered out

        dp = dp + fan_dp_calculate(ID_fan_vec, V);    % dp_fan is calculated and added to dp-matrix
        
        dp = dp.*M; % dp-matrix has now shape of mesh-matrix
        
        if verbosity > 1
            disp(dp)
        end

%   Flap-Elements: calculate angles of flaps
        ID_flap_1or0 = filter_matrix(ID, ID_Flap, ID_Flap + 1);                  % Positions of flaps are identified (0,1)
%         ID_flaps = filter_matrix(ID, ID_Flap, ID_Flap + 1);                      % flap-values are filtered
        solution = flap_alpha_calculate(dp, ID_flap_1or0, V, M);
        ID = ID - ID.*ID_flap_1or0 + solution.ID_flaps;
        
        SOL.ID = ID;
        dp = solution.dp ;
        dp = dp.*M;
        
        dp_rounded = round(dp,1);
        if verbosity > 0 | show_dp_1_0 >0
            disp("Pressure-Matrix")
            disp(dp_rounded)
        end
        
%% Power Consumption (if solution exists)
        if solution.possible ==1
            power = calculate_powerconsumption(dp, V, ID_fan_1or0);
        else 
            power = 9999999;
        end 
        SOL.power = power;

end

