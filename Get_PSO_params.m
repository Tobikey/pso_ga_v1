function [params] = Get_PSO_params(PSO_type, maxIT, nPop, Init_type)
%UNTITLED3 This Function creates and returns the PSO-Parameters according to the
%specified type.

%%     Definition of General Parameters

    params.MaxIt = maxIT;
    params.nPop = nPop;
    params.ShowIterInfo = false;  % Flag for Showing Iteration Information
    params.Init_type = Init_type;


%%      Definition of Specific Parameters

    if PSO_type =="clerk-kennedy-2002"
        kappa = 1;
        phi1 = 2.05;
        phi2 = 2.05;
        phi = phi1 + phi2;
        chi = 2*kappa /abs(2-phi-sqrt(phi^2-4*phi));

        params.w = chi;             % Inertia Coefficient 
        params.wdamp = 0.5;        % Damping Ratio of Intertia Coefficient
        params.c1 = chi*phi1;       % Personal Acceleration Coefficient
        params.c2 =chi*phi2;        % Global Acceleration Coefficient
        disp("PSO-clerk-kennedy-2002");
    elseif PSO_type == "PSO-w=1.2,wdamp=0.5"
        params.w = 1.2;             % Inertia Coefficient
        params.wdamp = 0.5;         % Damping Ratio of Intertia Coefficient
        params.c1 = 2;              % Personal Acceleration Coefficient
        params.c2 = 2;              % Global Acceleration Coefficient
        disp("PSO-default-wdamp=0,5");
    elseif PSO_type =="w=1.2,wdamp=0,99"
        params.w = 1.2;             % Inertia Coefficient
        params.wdamp = 0.99;        % Damping Ratio of Intertia Coefficient
        params.c1 = 2;              % Personal Acceleration Coefficient
        params.c2 = 2;              % Global Acceleration Coefficient
        disp("PSO-default-wdamp=0,99");
    else
%     Default-Parameters:
        params.w = 1;               % Inertia Coefficient
        params.wdamp = 0.99;        % Damping Ratio of Intertia Coefficient
        params.c1 = 2;              % Personal Acceleration Coefficient
        params.c2 = 2;              % Global Acceleration Coefficient
        disp("PSO-default");
    end 

end

