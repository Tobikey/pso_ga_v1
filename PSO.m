function out = PSO(network, problem, params)

    %% Definition of Problem 

    CostFunction = problem.CostFunction;     % Cost function is a function of x (n-dimensional)which returns the performance of x
    nVar = problem.nVar;                     % Number of Unknown (Decision-) Variables

    VarSize = [1 nVar];                      % Matrix Size of Decision Variables

    VarMin = problem.VarMin;                 % Lower Bound of Decision Variables
    VarMax = problem.VarMax;                 % Upper Bound of Decision Variables
    MaxVelocity = 0.2*(VarMax - VarMin);
    MinVelocity = - MaxVelocity;
    
    %% Parameters of PSO

    MaxIt = params.MaxIt;     % Maximum Number of Iterations

    nPop = params.nPop;       % Population Size (Swarm Size)

    w = params.w;           % Inertia Coefficient
    wdamp = params.wdamp;    % Damping Ratio of Intertia Coefficient
    c1 = params.c1;          % Personal Acceleration Coefficient
    c2 = params.c2;          % Global Acceleration Coefficient
    Init_type = params.Init_type; % Type of initialization of the pupulation
    
    % Flag for Showing iteration Information
    ShowIterInfo = params.ShowIterInfo; 

    %% Initialization

    %The Particle Template
    empty_particle.Position = []; 
    empty_particle.Velocity = [];
    empty_particle.Cost = [];
    empty_particle.Best.Position = [];
    empty_particle.Best.Cost = [];

    %Create Population Array
    particle = repmat(empty_particle, nPop, 1); % Creates Structure for the whole Population

    %Initialize Global Best
    GlobalBest.Cost = inf;                      % Starts with the worst Value
    
    %Initialize according to a specific demand..
    init_matrix = initialize_semi_random(network, params, problem);
    GlobalBest.Position = init_matrix(1,:);

    %Initialize Population Members
    for i=1:nPop

        %Generate Random Solution
%         particle(i).Position = unifrnd(VarMin, VarMax, VarSize);
        particle(i).Position = init_matrix(i,:);
        
        %Initialize Velocity
        particle(i).Velocity = zeros(VarSize);

        %Evaluation
        particle(i).Cost = CostFunction(network, particle(i).Position,0).Cost;

        % Update the Personal Best
        particle(i).Best.Position = particle(i).Position;
        particle(i).Best.Cost = particle(i).Cost;

        % Update Global Best
        if particle(i).Best.Cost < GlobalBest.Cost
           GlobalBest = particle(i).Best;
        end

    end
    % Array to HOld Best Cost Value of the optimization
    BestCosts = zeros(MaxIt, 1);


    %% Main Loop of PSO

    for it=1:MaxIt

        for i=1:nPop
            
%             particle(i).Best.Position
%             GlobalBest.Position
%             particle(i).Position
            % Update Velocity
            particle(i).Velocity = w*particle(i).Velocity ...
                + c1*rand(VarSize).*( particle(i).Best.Position - particle(i).Position)...
                + c1*rand(VarSize).*(GlobalBest.Position - particle(i).Position);
            
%             % Apply Velocity Limits
            particle(i).Velocity = max(particle(i).Velocity, MinVelocity);
            particle(i).Velocity = min(particle(i).Velocity, MaxVelocity);
            
            % Update Position
            particle(i).Position = particle(i).Position + particle(i).Velocity;
            
            % Apply Lower and Upper Bound Limits
            particle(i).Position = max(particle(i).Position, VarMin);
            particle(i).Position = min(particle(i).Position, VarMax);
            
            % Evaluation
            particle(i).Cost = CostFunction(network, particle(i).Position,0).Cost;

            % Update Personal Best
            if particle(i).Cost < particle(i).Best.Cost
                particle(i).Best.Position = particle(i).Position;
                particle(i).Best.Cost = particle(i).Cost;
            end

             % Update Global Best
            if particle(i).Cost < GlobalBest.Cost
                GlobalBest = particle(i).Best;
                CostFunction(network, GlobalBest.Position, 1).Cost;
            end

        end

        % Store the Best Cost Value
        BestCosts(it) = GlobalBest.Cost;

        % Display Iteration Information
        if ShowIterInfo
            disp(['Iteration' num2str(it) ' : Best Cost = ' num2str(BestCosts(it))]);
        end

        % Damping Inertia Coefficient
        w = w* wdamp;

    end
    
    GlobalBest.Position = ID_by_variables(GlobalBest.Position,network);
    out.pop = particle;
    out.GlobalBestSol = GlobalBest;
    out.BestCosts = BestCosts;
    CostFunction(network, GlobalBest.Position, 2).Cost;

end

