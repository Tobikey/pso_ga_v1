function [result] = calculate_powerconsumption(dp, V, ID_fan_1or0)
%CALCULATE_POWERCONSUMPTION This function takes the pressures and volume
%flows (and efficiencies) and calculates the total power consumption

% Physical 1D-eq: P1 = (100*V*dp)/eta
%   1. Calculate single power-consumption
%   2. Add all power consumptions up

%% Important variables

    rows = size(dp, 1);
    columns = size(dp,2);
    dp_sum = zeros(rows, 1);
    dp = collapse_mat_to_vec(dp);
       
% Main Loop
    eta = 1;
    V = (V.*(ID_fan_1or0));    
    P = abs((100.*V.*(-dp))/eta);
    
   result = sum(P);
   
   
   
   
   
   
   
   
   
   


end

