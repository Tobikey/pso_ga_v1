function [result] = mesh_to_graph(network)
%MESH_TO_GRAPH This function takes a mesh matrix and the ID-vector and
%displays a graph with the given values.
%   
mesh = network.M;
ID = network.ID;

ID_Res =   network.ID_Res;         % simple Tube-Resistance
ID_Empty = network.ID_Empty;       % empty => No Element is here
ID_Fan =   network.ID_Fan;         % Fan 
ID_Flap =  network.ID_Flap;        % Flap => to adjust dp


rows = size(mesh,1);
columns = size(mesh,2);

Adj_mat = zeros(columns, columns);

left = 1;
right = 0;

for row = 1:rows
    left =1;
    for column = 1:columns
%         disp(mesh(row, column));
        if mesh(row, column) ==1 && column > left
            right = column;
            Adj_mat(left, right) = 1;
            Adj_mat(right, left) = 1;
            left = right;
        end 
    end 
end

% disp(Adj_mat)

res_counter = 1;
empty_counter = 1;
flap_counter = 1;
fan_counter = 1;

for column = 1: columns
    ID_round = fix(ID(column));
    
    switch ID_round
        case ID_Res
            names{column} = convertStringsToChars("Resistance_"+ (num2str(res_counter)));
            res_counter = res_counter +1;
            
        case ID_Empty
            names{column} = convertStringsToChars("Empty_"+ (num2str(empty_counter)));
            empty_counter = empty_counter +1;

        case ID_Fan
            type = (fix(ID(column)*10)/10-fix(ID(column)))*10;
            type = "(" + num2str((type<4)*1 + (type <7 && type >=4)*2 + (type <10 && type >=7)*3 ) + ") ";
            percentage = ":" +num2str( fix(1000*((ID(column) - fix(ID(column)*10)/10 ))))+"%";
%             percentage = "." + num2str(fix((ID(column)-ID_round)*100))+ "%";
            names{column} = convertStringsToChars("Fan_"+ (num2str(fan_counter) + type   )+ percentage)   ;
            fan_counter = fan_counter +1;
        
        case ID_Flap
            names{column} = convertStringsToChars("Flap_" + (num2str(flap_counter))) ;   
            flap_counter = flap_counter +1;
        otherwise 
            names{column} = convertStringsToChars("Empty_" + (num2str(empty_counter)));
            empty_counter = empty_counter +1;
    
    end 
end 




figure(1);
G = graph(Adj_mat, names);
plot(G);
% pause on;
% pause(1);


result = 1;
end

