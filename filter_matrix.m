function [result] = filter_matrix(matrix,lower_bound, upper_bound)
%FILTER_VECTOR This function takes a vector and returns a vector of the same size.
% All Values inside the bounds are set to 1
% All values out of bounds are set to 0
% lower bound: includes value let
% upper bound: cutoff if value is bound

% Preliminary stuff
    rows = size(matrix,1);
    columns = size(matrix,2);
    
    result = zeros(rows, columns);
    
% Main Loop
    for row = 1: rows
        for column = 1: columns
            if (matrix(row, column) >=lower_bound ) && (matrix(row, column) <upper_bound)
                result(row, column) = 1;
            end 
        end 
    end 
        


end

