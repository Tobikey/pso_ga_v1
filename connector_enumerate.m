function [GlobalBest] = connector_enumerate(network, problem, Fan_speed_split)

    x_enumerated = enumerate(network, problem, Fan_speed_split);
    GlobalBest.Cost = 9999999;

    for row = 1: size(x_enumerated,1)
        solution = Cost_Function_connection(network, x_enumerated(row,:),  0);
    %     performance(row)= Cost_Function_connection(network, x_enumerated(row,:),  0).Cost;
        if solution.Cost < GlobalBest.Cost
            GlobalBest.Cost = solution.Cost;
            GlobalBest.ID = x_enumerated(row,:);
        end 

    end 

end

