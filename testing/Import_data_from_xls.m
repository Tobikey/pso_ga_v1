clc, %clears the screen
clear, % clears the workspace
close all, %closes all previously created figures 


% filepath = "C:\Users\tobia\OneDrive\Desktop\01_Studium\Master_Energietechnik\01_SS_2020\01_Studienarbeit\03_Matlab\Airflow_Optimization_Repository_V1\testing";
% 
filename = "C:\Users\tobia\OneDrive\Desktop\01_Studium\Master_Energietechnik\01_SS_2020\01_Studienarbeit\03_Matlab\Airflow_Optimization_Repository_V1\testing\Kennfeldmessung_G3G190.csv"
%  [filename, filepath] = uigetfile( '*.xls*' ) ;

% x = Data.x;
[Data]=xlsread(filename);
x = Data(:,1);
y = Data(:,2);
v = Data(:,3);
n = Data(:,4);

%  
F = scatteredInterpolant(x,y,v);

[xq,yq] = meshgrid(0:1:1000);
 tic
F.Method = 'natural';
v_natural = F(xq,yq);
toc

tic
F.Method = 'nearest';
v_nearest = F(xq,yq);
toc

tic
F.Method = 'linear';
v_linear = F(xq,yq);
toc






figure
    plotnr = 1; % iterator which is increased, after new subplot is created
    internr = 1; % iterator which is increased, after new interpolation is added
    
    subplot(2,3,plotnr)
        plotnr = plotnr +1;
%         plot3(x, y, v,'.')
        hold on
        surf(xq,yq,v_natural, 'EdgeAlpha', 0)
        title('natural Interpolant');
        hold off;

    subplot(2,3,plotnr)
        plotnr = plotnr +1;
        hold on
        surf(xq,yq,v_linear, 'EdgeAlpha', 0)
        title('linear Interpolant');
        hold off;
        
        
    subplot(2,3,plotnr)
        plotnr = plotnr +1;
        hold on
        surf(xq,yq,v_nearest, 'EdgeAlpha', 0)
        title('nearest neighbour');
        hold off;







