
clc; %clears the screen
clear; % clears the workspace
close all; %closes all previously created figures 

%% Original Sampling
    [X,Y] = meshgrid(-3:3);
    V = peaks(X,Y);
    % Plot the coarse sampling.

    figure
    plotnr = 1; % iterator which is increased, after new subplot is created
    internr = 1; % iterator which is increased, after new interpolation is added
    subplot(4,2,plotnr)
    plotnr = plotnr +1;
    surf(X,Y,V)
    title('Original Sampling');
    [Xq,Yq] = meshgrid(-3:0.25:3);
    
    %% comparison settings
    i_max = 100;

%% Default interpolation
interp_type = "default";
interp_name = interp_type + " interpolation";
toc_sum = 0;
for i = (1:i_max)
    tic
        Vq_default = interp2(X,Y,V,Xq,Yq);
        toc_sum = toc_sum + toc
end 
    disp(interp_name);
    names(internr) = interp_type;
    times(internr) = toc_sum/i_max;
    internr = internr +1;
%     figure     % Plot
    subplot(4,2,plotnr)
    plotnr = plotnr +1;
    surf(Xq,Yq,Vq_default);
    title(interp_name);

%% Cubic Interpolation
interp_type = "cubic";
interp_name = interp_type + " interpolation";
toc_sum = 0;
for i = (1:i_max)
    tic
        Vq_default = interp2(X,Y,V,Xq,Yq,interp_type);
        toc_sum = toc_sum + toc; 
end 
    disp(interp_name);
    names(internr) = interp_type;
    times(internr) = toc_sum/i_max;
    internr = internr +1;
%     figure     % Plot
    subplot(4,2,plotnr)
    plotnr = plotnr +1;
    surf(Xq,Yq,Vq_default);
    title(interp_name);
    
%% nearest Interpolation
interp_type = "nearest";
interp_name = interp_type + " interpolation";
toc_sum = 0;
for i = (1:i_max)
    tic
        Vq_default = interp2(X,Y,V,Xq,Yq,interp_type);
        toc_sum = toc_sum + toc; 
end 
    disp(interp_name);
    names(internr) = interp_type;
    times(internr) = toc_sum/i_max;
    internr = internr +1;
%     figure     % Plot
    subplot(4,2,plotnr)
    plotnr = plotnr +1;
    surf(Xq,Yq,Vq_default);
    title(interp_name);

%% nearest Interpolation
interp_type = "makima";
interp_name = interp_type + " interpolation";
toc_sum = 0;
for i = (1:i_max)
    tic
        Vq_default = interp2(X,Y,V,Xq,Yq,interp_type);
        toc_sum = toc_sum + toc; 
end 
    disp(interp_name);
    names(internr) = interp_type;
    times(internr) = toc_sum/i_max;
    internr = internr +1;
%     figure     % Plot
    subplot(4,2,plotnr)
    plotnr = plotnr +1;
    surf(Xq,Yq,Vq_default);
    title(interp_name);
    
    %% spline Interpolation
interp_type = "spline";
interp_name = interp_type + " interpolation";
toc_sum = 0;
for i = (1:i_max)
    tic
        Vq_default = interp2(X,Y,V,Xq,Yq,interp_type);
        toc_sum = toc_sum + toc; 
end 
    disp(interp_name);
    names(internr) = interp_type;
    times(internr) = toc_sum/i_max;
    internr = internr +1;
%     figure     % Plot
    subplot(4,2,plotnr)
    plotnr = plotnr +1;
    surf(Xq,Yq,Vq_default);
    title(interp_name);
    
    %% Plot times
    
    subplot(4,2,plotnr)
    plotnr = plotnr +1;
    
%     X = categorical({times(:,1)});
% X = reordercats(X,{'Small','Medium','Large','Extra Large'});
    
%     X = reordercats(X,{times(:,1)});

    bar(times)
%     bar(times(:,2))
%     plot(times(:,1),times(:,2));


