clc, %clears the screen
clear, % clears the workspace
close all, %closes all previously created figures 



%% data for eta fit
    % VZR 71-0200

%     fan1.dp = [67,100,200,300,500,
%         67,100,200,300,500,700,900,...
%         67,100,200,300,500,700,900,1200,1500,1700,...
%         200,300,500,700,900,1200,1500,1700,2000,...
%         300,500,700,900,1200,1500,1700,2000,...
%         500,700,900,1200,1500,1700,2000],
%     fan1.q = [1000,1000,1000,1000,1000,...
%         1500,1500,1500,1500,1500,1500,1500,...
%         2000,2000,2000,2000,2000,2000,2000,2000,2000,2000,...
%         3000,3000,3000,3000,3000,3000,3000,3000,3000,...
%         4000,4000,4000,4000,4000,4000,4000,4000,...
%         5000,5000,5000,5000,5000,5000,5000], 
    

%     [X,Y] = meshgrid(100:100:1100,1500:500:4500);
%     disp(fan1.mesh)

    
    
   
    fan1.eta = [68.00,63.00,69.10,68.00,64.00,00.00,00.00,00.00,00.00,00.00;
                42.00,47.00,61.50,67.00,70.00,68.00,66.00,00.00,00.00,00.00;
                30.00,35.50,50.00,57.50,66.00,68.50,70.00,68.00,66.50,65.00;
                32.50,41.50,53.00,59.80,64.00,67.00,68.20,69.00,70.00,00.00;
                30.00,40.00,48.50,53.00,58.00,63.50,65.00,66.50,00.00,00.00;
                32.00,38.50,45.00,49.50,55.00,57.50,60.00,00.00,00.00,00.00];
    
    tmp = zeros        
    
%     oldMatrix = fan1.eta  % The original matrix
%     newMatrix = zeros(size(fan1.mesh,1),size(fan1.mesh,2)) % The new matrix (with zeros)
%     newMatrix(1:size(fan1.eta,1), 1:size(fan1.eta,2)) = oldMatrix % Overlap the original matrix in the new matrix
%     
    


    X = 100:100:1100;
    Y = 1:11;
    Z = [1 2 3 4 0 0 0 0 0 0 0];
    interpolated = scatteredInterpolant(X',Y',Z');
%     surf(interpolated)
    
    
%     surf(newMatrix)
    
%     surf(fan1.mesh(1,:),fan1.mesh(:,1),newMatrix)
    
    
%     
%     
%     figure
%     
% %     subplot(3,1,1)
% % %     con(fan1.q,fan1.dp),
% % %     surf([fan1.q,fan1.dp],fan1.eta),
% %     contour([fan1.q,fan1.dp],fan1.eta),
% %     
% %     subplot(3,1,2)
% %     surf([fan2.q,fan2.dp],fan2.eta),
% %     
% %     subplot(3,1,3)
% %     surf([fan3.q,fan3.dp],fan3.eta),
%     
%     
%     
%     
    
    
    
        