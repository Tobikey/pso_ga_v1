function [init_matrix] = initialize_semi_random(network, params, problem)
%INITIALIZE_SEMI_RANDOM This Function creates an initialization matrix for


VarMin = problem.VarMin;
VarMax = problem.VarMax;
nVar = problem.nVar;
nPop = params.nPop;
Init_type = params.Init_type;
%the PSO. 

% if Init_type == "default"
%     
% end

if Init_type == "grid-init"
    
%   1. Number of possibilities to choose from (for now only (a) fan, (b) no fan, differentiation of different kinds of fan is possible later on.)
    n_types = ceil(VarMax - VarMin);
    
%   2. Get all Possible Combinations
    values = 1:n_types;                         %
    k = nVar;                                   %
    n = n_types;                          %//  number of values
    combs = values(dec2base(0:n^k-1,n)-'0'+1);  %//  generate all tuples
    combs = combs(all(diff(combs.')>=0, 1),:);  %'// keep only those that are sorted

%   3. Increase the Number of Combinations so that there are enough for the
%   whole population
    n_repeat = ceil(nPop / size(combs,1));
    
    combs = repmat(combs,n_repeat,1);  
    combs_lower = combs +3 ;
    combs_upper = combs_lower + 0.9999;
    
    rows_init_matrix = size(combs,1);
    
    init_matrix = unifrnd(combs_lower,combs_upper,[rows_init_matrix nVar]);
    
%     disp(init_matrix)
    disp("----------- Initialisierung -----------")
    disp(n_repeat + " - fache initialisierungs-Kombinationen in Polpulation")

    
elseif Init_type == "enumerate"
%     Fan_speed_increments = 7;
    Fan_speed_increments = n_by_n_over_k(nPop, nVar)-1;

%     Fan_speed_increments = ceil(log(nPop)/log(nVar));
    init_matrix = enumerate(network, problem, Fan_speed_increments);
    
elseif Init_type == "default"
    init_matrix = unifrnd(VarMin,VarMax,[nPop nVar]);
else 
    init_matrix = unifrnd(VarMin,VarMax,[nPop nVar]);
end 

% disp("enumerate-matrix: -------------");
% Fan_speed_increments = 7;
% disp(enumerate(network, problem, Fan_speed_increments));
% disp(size(enumerate(network, problem, Fan_speed_increments),1));
% disp("default_init-matrix: -------------");
% disp(size(unifrnd(VarMin,VarMax,[nPop nVar]),1));
% disp(unifrnd(VarMin,VarMax,[nPop nVar]));
init_matrix = init_matrix([1:nPop],:);

end

