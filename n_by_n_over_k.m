function [output] = n_by_n_over_k(n_combinations, k)
%N_K__1 Summary This function calculates the number of combinations with
%duplicates. n: number of options to choose from, k = number of times
%something is chosen.

% n_combinations = 
    for n_komb_guess = 1:1000
        output = factorial(n_komb_guess+k-1)/(factorial(n_komb_guess-1)*factorial(k));
        if output > n_combinations
%             n_komb_guess = n_komb_guess + 1;
%             output = factorial(n_komb_guess+k-1)/(factorial(n_komb_guess-1)*factorial(k));
            n_komb_guess
            n_combinations
            output = n_komb_guess;
            break;
    end 
end