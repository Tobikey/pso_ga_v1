function [result_vec] = collapse_mat_to_vec(input_mat)
%COLLAPSE_MAT_TO_VEC This function collapses a 2D-Matrix to a Vector. 
% Asumption: The matrix contains in every column: {0 , ID}
% There is only two different posibilities for a column.
% The function removes all 0's if possible and creates a vector with only
% {ID}

size_mat = size(input_mat);
rows_max = size_mat(1);
columns_max = size_mat(2);

result_vec = zeros(1, columns_max);

    for column = 1: columns_max
        for row = 1:rows_max
           if input_mat(row, column)~= 0
              result_vec(1, column) = input_mat(row, column);
           end
        end
    end

end

