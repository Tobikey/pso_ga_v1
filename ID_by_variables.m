function [ID] = ID_by_variables(x,network)
%ID_BY_VARIABLES Summary of this function goes here
%   Detailed explanation goes here

%     ID = network.ID;
        x_length = size(x,2);
        ID_length = size(network.ID,2) ;
        x_column = 1;
%     Going through ID from left to right, switching out the variable
        for column = 1:ID_length
            if (network.ID(1, column) >= min(network.ID_Empty, network.ID_Fan) && network.ID(1, column) < max(network.ID_Empty, network.ID_Fan) + 1)
                if (x(x_column) > network.ID_Empty ) && (x(x_column) < network.ID_Empty +1)
                    network.ID(1, column) = network.ID_Empty;
                else
                network.ID(1, column) =  x(x_column);
                x_column = x_column +1;
                end 
            end 
        end    

        ID = network.ID;



end

