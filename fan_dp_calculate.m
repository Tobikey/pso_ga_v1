
function [result] = fan_dp_calculate(ID_fan, V)
%FAN_DP_CALCULATE This function takes the ID_fan_vector and calculates all
%pressure differences for each fan and returns a vector with those values

%   Preliminary stuff
        rows = size(ID_fan,1);
        columns = size(ID_fan,2);
        result = zeros(rows, columns);

%   Calculation

%       Fan-Equation: dp = tp .*10 .*n - (tp.*10.*n)/ ((n./20).^2).*V.^2
        tp = 1;
        
        for column = 1:columns
            ID = ID_fan(1, column);
            type = (fix(ID*10)/10-fix(ID))*10;
            n = 1000*((ID - fix(ID*10)/10 ));
%             n2 = (fix(ID_fan(1, column)*10000000)/10000000 - fix(ID_fan(1, column)*100)/100)*10000; % This cuts off all digits except the power % of the fan
            if ID_fan(1, column) ~=0
%             disp("n:" + num2str(n) + " n2:" + num2str(n2) );
%             disp("ID:= " + num2str(ID)+ " , type:= " + num2str(type) + " , %:= " + num2str(n));
%             
            if type < 4
                tp = 1;
                result(1, column) =  - (tp .*10 .*n - (tp.*10.*n)/ ((n./20).^2).*V(1,column).^2 );
            
            if type < 7 && type >=4
                tp = 2;
                result(1, column) =  - (tp .*10 .*n - (tp.*10.*n)/ ((n./20).^2).*V(1,column).^2 );
    
                
                
            elseif type < 10 && type >=7
                tp = 3;
                result(1, column) =  - (tp .*10 .*n - (tp.*10.*n)/ ((n./20).^2).*V(1,column).^2 );
            end 
            
%             if ID_fan(1, column) ~=0
%                 n = (fix(ID_fan(1, column)*10000000)/10000000 - fix(ID_fan(1, column)*100)/100)*10000; % This cuts off all digits except the power % of the fan
%                 result(1, column) =  - (tp .*10 .*n - (tp.*10.*n)/ ((n./20).^2).*V(1,column).^2 );
%                 
%             end 
            end 
        end 

end

