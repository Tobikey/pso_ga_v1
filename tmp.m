% 
% % 
clc; %clears the screen
clear; % clears the workspace
close all; %closes all previously created figures 

Fan_speed_increments = 2;

VarMin = 3;
VarMax = 4.99;
nVar = 2;
ID_Fan = 4;
ID_Empty = 3;
ID = [ 1 3 4 5];

ID_enumerated = [];
    
%   1. Number of possibilities to choose from (for now only (a) fan, (b) no fan, differentiation of different kinds of fan is possible later on.)
% ID_Fan_list = linspace(ID_Fan + 1 / Fan_speed_increments, ID_Fan + 0.9999, Fan_speed_increments)
ID_Fan_list = linspace( 1 / Fan_speed_increments, 0.99, Fan_speed_increments)

% If different types of fan's exist:
multiple_fans = 2;
% if multiple_fans ==2
    types = [.1 .6];

    % size(types,2)
    i_total = 1;
    for i = 1:size(ID_Fan_list,2)
        for t = 1:size(types,2)
            ID_Fan_list_new(1, i_total) = types(1,t) + ID_Fan_list(i)/10
            i_total = i_total +1;
        end 
    end 
    ID_Fan_list = ID_Fan_list_new + ID_Fan;
% end 

ID_Fan_list = horzcat(ID_Fan_list, ID_Empty);

disp("ID-Fan-List");
disp(ID_Fan_list);
   
%   2. Get all Possible Combinations
    values = ID_Fan_list; 
       
    k = nVar;                                   %
  
    if numel(values)==1 
        n = values;
        combs = nchoosek(n+k-1,k);
    else
        n = numel(values);
        combs = bsxfun(@minus, nchoosek(1:n+k-1,k), 0:k-1);
        combs = reshape(values(combs),[],k);
    end
   
ID_enumerated = repmat(ID, size(combs,1), 1);
column_comb = 1;


init_matrix = combs

