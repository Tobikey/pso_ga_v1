function [init_matrix] = enumerate(network, problem, Fan_speed_increments)
%ENUMERATE This Function an enumeration matrix of all possible enumerations.
% 1. Level: All combinations are being created
% 2. Level: For each combination, all Fan-settings are being created (e.g. 0.1, 0.2, ... 1)

VarMin = problem.VarMin;
VarMax = problem.VarMax;
nVar = problem.nVar;
ID_Fan = network.ID_Fan;         % Fan 
ID_Empty = network.ID_Empty;
ID = network.ID;

ID_enumerated = [];
    
%   1. Number of possibilities to choose from (for now only (a) fan, (b) no fan, differentiation of different kinds of fan is possible later on.)
% ID_Fan_list = linspace(ID_Fan + 1 / Fan_speed_increments, ID_Fan + 0.9999, Fan_speed_increments);
ID_Fan_list = linspace( 1 / Fan_speed_increments, 0.99, Fan_speed_increments);

% If different types of fan's exist:
multiple_fans = 2;
% if multiple_fans ==2
    types = [.2 .5 .8];

    % size(types,2)
    i_total = 1;
    for i = 1:size(ID_Fan_list,2)
        for t = 1:size(types,2)
            ID_Fan_list_new(1, i_total) = types(1,t) + ID_Fan_list(i)/10;
            i_total = i_total +1;
        end 
    end 
    ID_Fan_list = ID_Fan_list_new + ID_Fan;
% end 




ID_Fan_list = horzcat(ID_Fan_list, ID_Empty);

disp("ID-Fan-List");
disp(ID_Fan_list);
   
%   2. Get all Possible Combinations
    values = ID_Fan_list; 
       
    k = nVar;                                   %
  
    if numel(values)==1 
        n = values;
        combs = nchoosek(n+k-1,k);
    else
        n = numel(values);
        combs = bsxfun(@minus, nchoosek(1:n+k-1,k), 0:k-1);
        combs = reshape(values(combs),[],k);
    end
   
ID_enumerated = repmat(ID, size(combs,1), 1);
column_comb = 1;

    disp("enum-kombinations: " + num2str(size(combs,1)));
%     disp("enum-kombinations");
%     disp(combs);
   init_matrix = combs;
   
end

