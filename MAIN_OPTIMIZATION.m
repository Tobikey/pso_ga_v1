clc; %clears the screen
clear; % clears the workspace
close all; %closes all previously created figures 
echo off;
%% Description: 
%This Script optimizes and calculates the arrangement and power-consumption of a ventilation-System.

%% Input of Geometry and Parameters

%---------Input-Part-------------

%     Mesh-Matrix:
%       network.M = [1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
%                    1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
%                    1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
%                    1 1 1 1 1  1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
%                    1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
%                    1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0
%                    1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 1 1 1 0 0 0 0 0
%                    1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 
%                    1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0
%                    1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 ];
% %     ID-Matrix: (2:Friction; 3: Fan, 4: empty, 5: Flap)
%       network.ID =[2 4 4 4 4 4 4 4 4 2 4 2 4 5 4 4 2 4 5 2 4 5 2 4 5 2 5 2 3 2 3 5 3 4 5 3 5 3 5 5];
% %     Volumeflow-Demand
%       network.k = [0.1; 0.1; 0.1; 0.3;0.5;1; 0.51; 0.1; 0.3; 0.5];


% %     Mesh-Matrix:
%       network.M = [1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 
%                    1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0 
%                    1 1 1 1 1 1 1 0 0 0 1 1 0 0 0 1 1 1 0 0 0 0 0 
%                    1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 
%                    1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 ];
% %     ID-Matrix: (2:Friction; 3: Fan, 4: empty, 5: Flap)
%       network.ID =[2 4 4 4 4 2 4 2  4 5 4 4 2 4 5 2 4 5 2 4 5 2 5];
% %     Volumeflow-Demand
%       network.k = [1; 1; 0.1; 0.3;0.5];
           
%     Mesh-Matrix:
      network.M = [1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0
                   1 1 1 1 1 0 0 0 1 1 1 1 1 0 0 0 0 0 0
                   1 1 1 1 1 0 0 0 1 1 0 0 0 1 1 1 0 0 0
                   1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1];
%     ID-Matrix: (2:Friction; 3: Fan, 4: empty, 5: Flap)
      network.ID = [2 4 4 2 4 2  4 5 4 4 2 4 5 2 4 5 2 4 5];
%     Volumeflow-Demand
      network.k = [1; 1; 1; 1];

% %     Mesh-Matrix:
%       network.M = [ 1 1 1  1 0 1 0 
%                     1 1 1  0 1 0 1  
%                       ];
% %     ID-Matrix: (2:Friction; 3: Fan, 4: empty, 5: Flap)
%       network.ID = [ 4 4  2  2 2 5 5];
% %     Volumeflow-Demand
%       network.k = [1; 1];
%                
%---------No Input Neccessary--------------
    
%     ID-Convention
      network.ID_Res = 2;         % simple Tube-Resistance
      network.ID_Empty = 3;       % empty => No Element is here
      network.ID_Fan = 4;         % Fan 
      network.ID_Flap = 5;        % Flap => to adjust dp
      mesh_to_graph(network);
%     Cost-Function Naming 
      problem.CostFunction = @(network, x, disp) Cost_Function_connection(network, x, disp);
        
%     Calculating of number of "flexible" positions in network (Ranges of Empty and Fan are flexible)
      columns = size(network.ID,2);
      tmp = 0;
      for column = 1:columns
          if (network.ID(column) >= min(network.ID_Fan, network.ID_Empty)) && (network.ID(column) < max( network.ID_Empty, network.ID_Fan) +1)
              tmp = tmp + 1;
          end
      end
        problem.nVar = tmp;                     %     Number of Unknown (Decision-) Variables
        
%     Range of variable
      problem.VarMin = 3.0;                      % Lower Bound of Decision Variables
      problem.VarMax = 4.999;                            % Upper Bound of Decision Variables

%---------Simulation Parameters----------
% profile on

%     maximum Number of Iterations
      MaxIt = 200;
%     Number of elements in 1 population
      nPop = 2000;
%     Type of Optimization
      PSO_type = "clerk-kennedy-2002";
%     PSO_type = "standard"

%     Specification of Initialization type
      params.Init_type = "default";
      Init_type = "random-equal-combinations";
       
%     Create Optimization Parameters
      params = Get_PSO_params(PSO_type, MaxIt, nPop, Init_type);
      
%% Optimization
costs_all = [];
labels_all = [];

global vals;   % Global Variable to store Values of iteration in
global iter; 
vals = [];
iter = 1;

% ------------ Enumeration ------------
GlobalBest_enum_all = [];
GlobalBest_enum_all_title = [];
GlobalBest_enum_all_costs = [];
Elapsed_time = [];

disp("start_enumerations--------");
tic
Fan_speed_split = 3;
GlobalBest_enum = connector_enumerate(network, problem, Fan_speed_split);
disp(GlobalBest_enum.Cost);
GlobalBest_enum_all_costs = vertcat(GlobalBest_enum_all_costs, GlobalBest_enum.Cost);
GlobalBest_enum_all_title = vertcat(GlobalBest_enum_all_title, "Enum-split-" + num2str(Fan_speed_split));
Elapsed_time = vertcat(Elapsed_time, toc);
disp("enumeration:");
toc

% % Fan_speed_increments = ceil(log(nPop)/log(nVar));
% tic
% Fan_speed_split = 15;
% Fan_speed_split = fix((n_by_n_over_k(nPop * MaxIt, problem.nVar)-1)/3 +1) % calculates split with 2 types of fan
% GlobalBest_enum = connector_enumerate(network, problem, Fan_speed_split);
% disp(GlobalBest_enum.Cost);
% GlobalBest_enum_all_costs = vertcat(GlobalBest_enum_all_costs, GlobalBest_enum.Cost);
% GlobalBest_enum_all_title = vertcat(GlobalBest_enum_all_title, "Enum-split-" + num2str(Fan_speed_split));
% Elapsed_time = vertcat(Elapsed_time, toc);
% % 
% tic
% Fan_speed_split = 12;
% GlobalBest_enum = connector_enumerate(network, problem, Fan_speed_split);
% disp(GlobalBest_enum.Cost);
% GlobalBest_enum_all_costs = vertcat(GlobalBest_enum_all_costs, GlobalBest_enum.Cost);
% GlobalBest_enum_all_title = vertcat(GlobalBest_enum_all_title, "Enum-split-" + num2str(Fan_speed_split));
% Elapsed_time = vertcat(Elapsed_time, toc);

global vals 
vals= [];   % Global Variable to store Values of iteration in

      problem.VarMin = 3.95;                      % Lower Bound of Decision Variables
      problem.VarMax = 4.999;                            % Upper Bound of Decision Variables

      PSO_type = "clerk-kennedy-2002";
      Init_type = "default";
      params = Get_PSO_params( PSO_type, MaxIt, nPop, Init_type);
      
      Optimized = PSO(network, problem, params);
      BestSol = Optimized.GlobalBestSol;
      costs_all = horzcat(costs_all, Optimized.BestCosts);
      labels_all = horzcat(labels_all, "PSO-fan-favored," + PSO_type + Init_type);
      
        
%       PSO_type = "clerk-kennedy-2002";
%       Init_type = "default";
%       params = Get_PSO_params( PSO_type, MaxIt, nPop, Init_type);
%       Optimized = PSO(network, problem, params);
%       
%       BestSol = Optimized.GlobalBestSol;
%       costs_all = horzcat(costs_all, Optimized.BestCosts);
%       labels_all = horzcat(labels_all, "PSO-fan-favored," + PSO_type + Init_type);
%       
%       PSO_type = "PSO-w=1.2,wdamp=0.5";
%       Init_type = "default";
%       params = Get_PSO_params(PSO_type, MaxIt, nPop, Init_type);
%       Optimized = PSO(network, problem, params);
%       BestSol = Optimized.GlobalBestSol;
%       costs_all = horzcat(costs_all, Optimized.BestCosts);
%       labels_all = horzcat(labels_all, "PSO-w=1.2,wdamp=0.5" + PSO_type + Init_type);      
%       
%       PSO_type = "w=1.2,wdamp=0,99";
%       Init_type = "default";
%       params = Get_PSO_params(PSO_type, MaxIt, nPop, Init_type);
%       Optimized = PSO(network, problem, params);
%       BestSol = Optimized.GlobalBestSol;
%       costs_all = horzcat(costs_all, Optimized.BestCosts);
%       labels_all = horzcat(labels_all, "PSO-w=1.2,wdamp=0,99" + PSO_type + Init_type);  
% % 
      
      %% GA - Optimization
 
vals= [];   % Global Variable to store Values of iteration in
iter = 1; 
        Init_type = "default";
        params = Get_PSO_params(PSO_type, MaxIt, nPop, Init_type);
        BestCostsGA = GA_connector(network, problem, params);
        costs_all = horzcat(costs_all, BestCostsGA);
        labels_all = horzcat(labels_all, "GA, " + Init_type + " Init");

%% Result Section

      figure(4);
    
      plot( costs_all(:,1), 'Linewidth',2,'DisplayName',labels_all(1));
      hold on
      legend
      i = 2;
      
      while i <= size(labels_all,2)
            plot( costs_all(:,i), 'Linewidth',2,'DisplayName',labels_all(i));
            i = i + 1;
      end 
      
      i = 1;
      
      while i <= size(GlobalBest_enum_all_costs,1)
            yline(GlobalBest_enum_all_costs(i),'DisplayName', GlobalBest_enum_all_title(i) ,'LineWidth',2, 'color',rand(1,3), 'LineStyle', '--');
            i = i + 1;
      end 
      
            
      hold off
      
      xlabel('Iteration');
      ylabel('BestCosts');
  