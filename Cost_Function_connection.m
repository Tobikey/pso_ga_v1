function z = Cost_Function_connection(network, x,  plot_1_or_0)
global vals;
global iter;

%     dp-matrix is only shown 1x, power is shown more often 
        show_dp_1_0 = plot_1_or_0-1;       % This is a parameter to activate printing all pressure differences => Good way of showing if flaps are opened or closed
        show_dp_1_0 = 0; % Dusplay of dp-Matrix is turned off.
        
%     Renaming of variables which describe the network, demand etc.
        M = network.M;
        ID = network.ID;
        k = network.k;
            
%% Creation of new ID 
        x_length = size(x,2);
        ID_length = size(ID,2) ;
        x_column = 1;
%     Going through ID from left to right, switching out the variable
        for column = 1:ID_length
            if (ID(1, column) >= min(network.ID_Empty, network.ID_Fan) && ID(1, column) < max(network.ID_Empty, network.ID_Fan) + 1)
                if (x(x_column) > network.ID_Empty ) && (x(x_column) < network.ID_Empty +1)
                    ID(1, column) = network.ID_Empty;
                else
                ID(1, column) =  x(x_column);
                x_column = x_column +1;
                end 
            end 
        end    

        network.ID = ID;
        
%% Calculation of Power 
        SOL = Cost_Function_calculation(network, show_dp_1_0);
%         disp("CostFunc_connection, ID:");
%         disp(network.ID);
        
        if plot_1_or_0 >= 1                          % Currently Best Solution

            disp("Power " + num2str(SOL.power));
                            
            mesh_to_graph(network);   
%             if plot_1_or_0 > 1                      % Overall Best solution (in Interval)
%                 disp("ID:")
%                 disp(SOL.ID);
%             end 
        end 
        
        vals(iter,1) = SOL.power;     
        iter = iter + 1;
        
        z.ID = network.ID;
        z.Cost = SOL.power;


end